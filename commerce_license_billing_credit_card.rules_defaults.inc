<?php

/**
 * @file
 * Rules integration for the Commerce License Billing credit card module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_license_billing_credit_card_default_rules_configuration() {

  $items['rules_commerce_license_billing_cc_charge_recurring_order'] = entity_import('rules_config', '{ "rules_commerce_license_billing_cc_charge_recurring_order" : {
    "LABEL" : "Charge a recurring order",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce License Billing Credit Card" ],
    "REQUIRES" : [
      "rules",
      "commerce_payment",
      "commerce_license_billing_credit_card",
      "commerce_cardonfile",
      "entity"
    ],
    "ON" : { "commerce_order_update" : [] },
    "IF" : [
      { "NOT data_is" : {
          "data" : [ "commerce-order:status" ],
          "value" : [ "commerce-order-unchanged:status" ]
        }
      },
      { "data_is" : {
          "data" : [ "commerce-order:status" ],
          "value" : "recurring_payment_pending"
        }
      },
      { "commerce_payment_order_balance_comparison" : {
          "commerce_order" : [ "commerce_order" ],
          "operator" : "\u003E",
          "value" : "0"
        }
      },
      { "commerce_license_billing_credit_card_recuring_condition" : { "commerce_order" : [ "commerce_order" ] } }
    ],
    "DO" : [
      { "commerce_cardonfile_order_select_card" : {
          "USING" : { "order" : [ "commerce-order" ] },
          "PROVIDE" : { "select_card_response" : { "select_card_response" : "Select card response" } }
        }
      },
      { "commerce_cardonfile_order_charge_card" : {
          "USING" : {
            "order" : [ "commerce-order" ],
            "charge" : [ "" ],
            "select_card_response" : [ "select_card_response" ],
            "card_data" : [ "" ]
          },
          "PROVIDE" : { "charge_card_response" : { "charge_card_response" : "charge Card Response" } }
        }
      }
    ]
  }
}');
//  $items['rules_commerce_license_billing_cc_charge_recurring_order'] = entity_import('rules_config', '{ "rules_commerce_license_billing_cc_charge_recurring_order" : {
//      "LABEL" : "Charge a recurring order if necessary",
//      "PLUGIN" : "reaction rule",
//      "TAGS" : [ "Commerce License Billing Credit Card" ],
//      "REQUIRES" : [ "rules", "commerce_payment", "commerce_cardonfile", "entity" ],
//      "ON" : [ "commerce_order_update" ],
//      "IF" : [
//        { "NOT data_is" : {
//            "data" : [ "commerce-order:status" ],
//            "value" : [ "commerce-order-unchanged:status" ]
//          }
//        },
//        { "data_is" : {
//            "data" : [ "commerce-order:status" ],
//            "value" : "recurring_payment_pending"
//          }
//        },
//        { "commerce_payment_order_balance_comparison" : {
//            "commerce_order" : [ "commerce_order" ],
//            "operator" : "\u003E",
//            "value" : "0"
//          }
//        },
//      ],
//      "DO" : [
//        { "commerce_cardonfile_order_select_card" : {
//            "USING" : { "order" : [ "commerce-order" ] },
//            "PROVIDE" : { "select_card_response" : { "select_card_response" : "Select card response" } }
//          }
//        },
//        { "commerce_cardonfile_order_charge_card" : {
//            "USING" : {
//              "order" : [ "commerce-order" ],
//              "charge" : [ "" ],
//              "select_card_response" : [ "select_card_response" ],
//              "card_data" : [ "" ]
//            },
//            "PROVIDE" : { "charge_card_response" : { "charge_card_response" : "charge Card Response" } }
//          }
//        }
//      ]
//    }
//  }');
  //$items['rules_commerce_license_billing_cc_update_order_charged'] = entity_import('rules_config', '{ "rules_commerce_license_billing_cc_update_order_charged" : {
  //    "LABEL" : "Update status of successfully charged recurring order",
  //    "PLUGIN" : "reaction rule",
  //    "WEIGHT" : "11",
  //    "TAGS" : [ "Commerce License Billing", "Commerce License Billing Credit Card" ],
  //    "REQUIRES" : [ "commerce_order", "commerce_cardonfile" ],
  //    "ON" : [ "commerce_cardonfile_charge_success" ],
  //    "DO" : [
  //      { "commerce_order_update_status" : {
  //          "commerce_order" : [ "order" ],
  //          "order_status" : "completed"
  //        }
  //      }
  //    ]
  //  }
  //}');

  return $items;
}
