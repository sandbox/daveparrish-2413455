<?php
/**
 * @file
 * Rules integration for the Commerce License Billing Credit Card module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_license_billing_credit_card_rules_condition_info() {
  $conditions['commerce_license_billing_credit_card_recuring_condition'] = array(
    'label' => t('Order is to be paid for with credit card.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce License Billing Credit Card'),
    'callbacks' => array(
      'execute' => 'commerce_license_billing_credit_card_recuring_condition',
    ),
  );
  return $conditions;
}

/**
 * Rules condition callback: check if an order is a cc recuring payment.
 */
function commerce_license_billing_credit_card_recuring_condition($order) {
  if (isset($order->data['first_payment_method'])) {
    return _commerce_license_billing_credit_card_is_payment_method_cardonfile($order->data['first_payment_method']);
  }

  return FALSE;
}
